/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.double_Linked;

/**
 *
 * @author informatics
 */
class DoublyLinkedApp {
    public static void main(String[] args) {
        // Create a new list
        DoublyLinkedList theList = new DoublyLinkedList();

        // Insert different items at the front and rear of the list
        theList.insertFirst(10);
        theList.insertFirst(30);
        theList.insertFirst(50);
        theList.insertLast(5);
        theList.insertLast(25);
        theList.insertLast(45);

        // Display the list forward and backward
        theList.displayForward();
        theList.displayBackward();

        // Delete the first, last, and an item with key 5
        theList.deleteFirst();
        theList.deleteLast();
        theList.deleteKey(5);

        // Display the list forward after deletions
        theList.displayForward();

        // Insert 100 after 10 and 200 after 25
        theList.insertAfter(10, 100);
        theList.insertAfter(25, 200);

        // Display the list forward after insertions
        theList.displayForward();
    } // end main()
} // end class DoublyLinkedApp


