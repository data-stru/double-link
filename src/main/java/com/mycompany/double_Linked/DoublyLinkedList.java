/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.double_Linked;

/**
 *
 * @author informatics
 */
public class DoublyLinkedList {
    private Link first; // Reference to the first item on the list
    private Link last;  // Reference to the last item on the list

    // Constructor
    public DoublyLinkedList() {
        first = null; // No items on the list yet
        last = null;
    }

    // Check if the list is empty
    public boolean isEmpty() {
        return first == null;
    }

    // Insert a new link at the front of the list
    public void insertFirst(long dd) {
        Link newLink = new Link(dd); // Create a new link

        if (isEmpty()) { // If the list is empty
            last = newLink; // newLink <-- last
        } else {
            first.previous = newLink; // newLink <-- old first
        }

        newLink.next = first; // newLink --> old first
        first = newLink; // first --> newLink
    }

    // Insert a new link at the end of the list
    public void insertLast(long dd) {
        Link newLink = new Link(dd); // Create a new link

        if (isEmpty()) { // If the list is empty
            first = newLink; // first --> newLink
        } else {
            last.next = newLink; // old last --> newLink
            newLink.previous = last; // old last <-- newLink
        }

        last = newLink; // newLink <-- last
    }

    // Delete the first link from the list (assumes non-empty list)
    public Link deleteFirst() {
        Link temp = first; // Save the first link

        if (first.next == null) { // If there's only one item
            last = null; // last --> null
        } else {
            first.next.previous = null; // null <-- old next
        }

        first = first.next; // first --> old next
        return temp;
    }

    // Delete the last link from the list (assumes non-empty list)
    public Link deleteLast() {
        Link temp = last; // Save the last link

        if (first.next == null) { // If there's only one item
            first = null; // first --> null
        } else {
            last.previous.next = null; // null <-- old previous
        }

        last = last.previous; // old previous <-- last
        return temp;
    }

    // Insert a new link after a link with a given key
    public boolean insertAfter(long key, long dd) {
        Link current = first; // Start at the beginning

        while (current.dData != key) { // Until a match is found
            current = current.next; // Move to the next link

            if (current == null) {
                return false; // Didn't find it
            }
        }

        Link newLink = new Link(dd); // Create a new link

        if (current == last) { // If it's the last link
            newLink.next = null; // newLink --> null
            last = newLink; // newLink <-- last
        } else {
            newLink.next = current.next; // newLink --> old next
            current.next.previous = newLink; // newLink <-- old next
        }

        newLink.previous = current; // old current <-- newLink
        current.next = newLink; // old current --> newLink
        return true; // Found it, did insertion
    }

    // Delete an item with a given key
    public Link deleteKey(long key) {
        Link current = first; // Start at the beginning

        while (current.dData != key) { // Until a match is found
            current = current.next; // Move to the next link

            if (current == null) {
                return null; // Didn't find it
            }
        }

        if (current == first) { // If it's the first item
            first = current.next; // first --> old next
        } else {
            current.previous.next = current.next; // old previous --> old next
        }

        if (current == last) { // If it's the last item
            last = current.previous; // old previous <-- last
        } else {
            current.next.previous = current.previous; // old next <-- old previous
        }

        return current; // Return the deleted link
    }

    // Display the list from the first to the last
    public void displayForward() {
        System.out.print("List (first-->last): ");
        Link current = first; // Start at the beginning

        while (current != null) { // Until the end of the list
            current.displayLink(); // Display data
            current = current.next; // Move to the next link
        }

        System.out.println();
    }

    // Display the list from the last to the first
    public void displayBackward() {
        System.out.print("List (last-->first): ");
        Link current = last; // Start at the end

        while (current != null) { // Until the start of the list
            current.displayLink(); // Display data
            current = current.previous; // Move to the previous link
        }

        System.out.println();
    }
} // end class DoublyLinkedList

