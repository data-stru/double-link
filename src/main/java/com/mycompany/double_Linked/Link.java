/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.double_Linked;

/**
 *
 * @author informatics
 */
class Link {
    public long dData;      // data item
    public Link next;       // next link in the list
    public Link previous;   // previous link in the list

    // Constructor
    public Link(long d) {
        dData = d;
        next = null;
        previous = null;
    }

    // Display the data in this link
    public void displayLink() {
        System.out.print(dData + " ");
    }
} // end class Link

