/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.implement_linked;

/**
 *
 * @author informatics
 */
class SortedList {
    private Link first; // Reference to the first item on the list

    // Constructor
    public SortedList() {
        first = null; // No items on the list yet
    }

    // Check if the list is empty
    public boolean isEmpty() {
        return (first == null);
    }

    // Insert a new link in order
    public void insert(long key) {
        Link newLink = new Link(key); // Create a new link
        Link previous = null; // Start at first
        Link current = first;

        // Traverse the list until the end or key <= current.dData
        while (current != null && key > current.dData) {
            previous = current;
            current = current.next; // Move to the next item
        }

        if (previous == null) { // At the beginning of the list
            first = newLink; // first --> newLink
        } else { // Not at the beginning
            previous.next = newLink; // old prev --> newLink
        }

        newLink.next = current; // newLink --> old current
    }

    // Remove and return the first link from the list (assumes non-empty list)
    public Link remove() {
        Link temp = first; // Save the first link
        first = first.next; // Delete the first link
        return temp; // Return the value
    }

    // Display the entire list
    public void displayList() {
        System.out.print("List (first-->last): ");
        Link current = first; // Start at the beginning of the list

        while (current != null) { // Until the end of the list
            current.displayLink(); // Print data
            current = current.next; // Move to the next link
        }

        System.out.println(); // Print a new line
    }
} // end class SortedList
