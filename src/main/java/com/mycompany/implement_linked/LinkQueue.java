/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.implement_linked;

/**
 *
 * @author informatics
 */
class LinkQueue {
    private FirstLastList theList;

    // Constructor
    public LinkQueue() {
        theList = new FirstLastList(); // Create a two-ended list
    }

    // Check if the queue is empty
    public boolean isEmpty() {
        return theList.isEmpty();
    }

    // Insert an item at the rear of the queue
    public void insert(long j) {
        theList.insertLast(j);
    }

    // Remove an item from the front of the queue
    public long remove() {
        return theList.deleteFirst();
    }

    // Display the entire queue
    public void displayQueue() {
        System.out.print("Queue (front-->rear): ");
        theList.displayList();
    }
} // end class LinkQueue
