/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.implement_linked;

/**
 *
 * @author informatics
 */
public class Link {
    public long dData;   // data item
    public Link next;    // next link in list

    // Constructor
    public Link(long dd) {
        dData = dd;
        next = null;     // Initialize the 'next' reference to null
    }

    // Display the data in the link
    public void displayLink() {
        System.out.print(dData + " ");
    }
}
