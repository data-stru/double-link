/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.implement_linked;

/**
 *
 * @author informatics
 */
class SortedListApp {
    public static void main(String[] args) {
        // Create a new list
        SortedList theSortedList = new SortedList();

        // Insert two items into the list
        theSortedList.insert(20);
        theSortedList.insert(40);

        // Display the list
        theSortedList.displayList();

        // Insert three more items into the list
        theSortedList.insert(10);
        theSortedList.insert(30);
        theSortedList.insert(50);

        // Display the list again
        theSortedList.displayList();

        // Remove an item from the list
        theSortedList.remove();

        // Display the list after removal
        theSortedList.displayList();
    } // end main()
} // end class SortedListApp

