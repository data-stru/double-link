/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.implement_linked;

/**
 *
 * @author informatics
 */
class LinkList {
    private Link first; // Reference to the first item on the list

    // Constructor
    public LinkList() {
        first = null; // No items on the list yet
    }

    // Check if the list is empty
    public boolean isEmpty() {
        return (first == null);
    }

    // Insert a new link at the beginning of the list
    public void insertFirst(long dd) {
        // Create a new link
        Link newLink = new Link(dd);

        // Make newLink point to the old first link
        newLink.next = first;

        // Update the first reference to newLink
        first = newLink;
    }

    // Delete the first item from the list (assuming the list is not empty)
    public long deleteFirst() {
        // Save a reference to the first link
        Link temp = first;

        // Update first to point to the old next link
        first = first.next;

        // Return the data from the deleted link
        return temp.dData;
    }

    // Display the entire list
    public void displayList() {
        Link current = first; // Start at the beginning of the list

        while (current != null) { // Until the end of the list
            current.displayLink(); // Print data
            current = current.next; // Move to the next link
        }

        System.out.println(); // Print a new line
    }
} // end class LinkList

