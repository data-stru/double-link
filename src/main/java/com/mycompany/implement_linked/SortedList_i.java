/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.implement_linked;

/**
 *
 * @author informatics
 */
class SortedList_i {
    private Link first; // Reference to the first item on the list

    // Constructor (no arguments)
    public SortedList_i() {
        first = null; // Initialize the list
    }

    // Constructor (takes an array of links as an argument)
    public SortedList_i(Link[] linkArr) {
        first = null; // Initialize the list

        // Copy elements from the array to the list in order
        for (int j = 0; j < linkArr.length; j++)
            insert(linkArr[j]); // Insert each link into the list
    }

    // Insert a link into the list (in order)
    public void insert(Link k) {
        Link previous = null; // Start at the first link
        Link current = first;

        // Traverse the list until the end or k.dData <= current.dData
        while (current != null && k.dData > current.dData) {
            previous = current;
            current = current.next; // Move to the next item
        }

        if (previous == null) { // At the beginning of the list
            first = k; // first --> k
        } else { // Not at the beginning
            previous.next = k; // old prev --> k
        }

        k.next = current; // k --> old current
    }

    // Remove and return the first link from the list (assumes non-empty list)
    public Link remove() {
        Link temp = first; // Save the first link
        first = first.next; // Delete the first link
        return temp; // Return the value
    }
}

