/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.implement_linked;

/**
 *
 * @author informatics
 */
class LinkStackApp {
    public static void main(String[] args) {
        LinkStack theStack = new LinkStack(); // Create a stack

        // Push items onto the stack
        theStack.push(20);
        theStack.push(40);

        // Display the stack
        theStack.displayStack();

        // Push more items onto the stack
        theStack.push(60);
        theStack.push(80);

        // Display the stack again
        theStack.displayStack();

        // Pop items from the stack
        theStack.pop();
        theStack.pop();

        // Display the stack after popping
        theStack.displayStack();
    } // end main()
} // end class LinkStackApp

