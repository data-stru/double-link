/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.implement_linked;

/**
 *
 * @author informatics
 */
class FirstLastList {
    private Link first; // Reference to the first item
    private Link last; // Reference to the last item

    // Constructor
    public FirstLastList() {
        first = null; // No items on the list yet
        last = null;
    }

    // Check if the list is empty
    public boolean isEmpty() {
        return first == null;
    }

    // Insert a new link at the end of the list
    public void insertLast(long dd) {
        Link newLink = new Link(dd); // Create a new link

        if (isEmpty()) { // If the list is empty
            first = newLink; // first --> newLink
        } else {
            last.next = newLink; // old last --> newLink
        }

        last = newLink; // newLink <-- last
    }

    // Delete the first link from the list (assuming it's not empty)
    public long deleteFirst() {
        long temp = first.dData;

        if (first.next == null) { // If there's only one item
            last = null; // null <-- last
        }

        first = first.next; // first --> old next
        return temp;
    }

    // Display the entire list
    public void displayList() {
        Link current = first; // Start at the beginning

        while (current != null) { // Until the end of the list
            current.displayLink(); // Print data
            current = current.next; // Move to the next link
        }

        System.out.println(); // Print a new line
    }
} // end class FirstLastList

