/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.implement_linked;

/**
 *
 * @author informatics
 */
class LinkQueueApp {
    public static void main(String[] args) {
        LinkQueue theQueue = new LinkQueue();

        // Insert items into the queue
        theQueue.insert(20);
        theQueue.insert(40);

        // Display the queue
        theQueue.displayQueue();

        // Insert more items into the queue
        theQueue.insert(60);
        theQueue.insert(80);

        // Display the queue again
        theQueue.displayQueue();

        // Remove items from the queue
        theQueue.remove();
        theQueue.remove();

        // Display the queue after removal
        theQueue.displayQueue();
    } // end main()
} // end class LinkQueueApp

