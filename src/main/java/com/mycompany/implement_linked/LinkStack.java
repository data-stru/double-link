/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.implement_linked;

/**
 *
 * @author informatics
 */
class LinkStack {
    private LinkList theList;

    // Constructor
    public LinkStack() {
        theList = new LinkList();
    }

    // Push an item onto the top of the stack
    public void push(long j) {
        theList.insertFirst(j);
    }

    // Pop an item from the top of the stack
    public long pop() {
        return theList.deleteFirst();
    }

    // Check if the stack is empty
    public boolean isEmpty() {
        return theList.isEmpty();
    }

    // Display the stack
    public void displayStack() {
        System.out.print("Stack (top-->bottom): ");
        theList.displayList();
    }
} // end class LinkStack
