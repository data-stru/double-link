/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.implement_linked;

/**
 *
 * @author informatics
 */
class ListInsertionSortApp {
    public static void main(String[] args) {
        int size = 10;

        // Create an array of links and fill it with random numbers
        Link[] linkArray = new Link[size];
        for (int j = 0; j < size; j++) {
            int n = (int) (java.lang.Math.random() * 99);
            Link newLink = new Link(n);
            linkArray[j] = newLink;
        }

        // Display the unsorted array contents
        System.out.print("Unsorted array: ");
        for (int j = 0; j < size; j++)
            System.out.print(linkArray[j].dData + " ");
        System.out.println();

        // Create a new sorted list initialized with the array
        SortedList_i theSortedList = new SortedList_i(linkArray);

        // Transfer links from the list to the array
        for (int j = 0; j < size; j++)
            linkArray[j] = theSortedList.remove();

        // Display the sorted array contents
        System.out.print("Sorted Array: ");
        for (int j = 0; j < size; j++)
            System.out.print(linkArray[j].dData + " ");
        System.out.println();
    } // end main()
} // end class ListInsertionSortApp

